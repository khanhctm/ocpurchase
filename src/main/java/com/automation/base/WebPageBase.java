package com.automation.base;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.automation.core.WebApp;

abstract public class WebPageBase {
	protected WebDriver driver;
	protected WebApp app;
	protected Logger log = Logger.getLogger(this.getClass());

	abstract protected void init(Object[] params) throws Exception;

	abstract protected void checkOnPage() throws Exception;

	// Constructor
	public WebPageBase() throws Exception {
	}

	public void initPageObject(Object[] params) throws Exception {
		// call init in derived class
		init(params);
		// call checkOnPage in derived class
		checkOnPage();
	}

}
