package com.automation.base;

import java.util.concurrent.ConcurrentHashMap;

import cucumber.api.Scenario;

public class TestContextManager {
	// context per thread
	private static final ConcurrentHashMap<Long, TestContext> _contexts = new ConcurrentHashMap<>();

	public static void createContext(Scenario scenario)
			throws Exception {
		Long threadId = Thread.currentThread().getId();
		if (_contexts.containsKey(threadId)) {
			_contexts.remove(threadId);
		}
		_contexts.put(threadId, new TestContext(scenario));
	}

	// Gets test context for current thread
	public static TestContext getTestContext() {
		Long threadId = Thread.currentThread().getId();
		return _contexts.get(threadId);
	}

	/**
	 * Get the name of the test environment from environment configuration
	 */
	public static TestEnvironments getTestEnvironmentFromConfig() throws Exception {
		String env = null;
		try {
			env = System.getProperty("AT_ENVIRONMENT");
			if (env == null) {
				env = System.getenv("AT_ENVIRONMENT");
			}
			// This is to be able to have unit tests without test env setting
			if (env == null) {
				System.out.println("No environment setting found");
				return null;
			}
			System.out.println("Environment detected: " + env.toUpperCase());
			return TestEnvironments.valueOf(env.toUpperCase());
		} catch (Exception e) {
			throw new Exception("The given test env: " + env + " is not supported.", e);
		}
	}

}