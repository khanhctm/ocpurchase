package com.automation.core;

import java.io.File;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;

import com.automation.base.ElementActions;
import com.automation.base.TestContextManager;
import com.automation.base.TestEnvironments;
import com.automation.base.WebPageBase;
import com.google.common.base.Function;
import com.automation.core.BrowserContext;
public class WebApp {
	//protected static WebDriver driver = null;
	private Logger log = Logger.getLogger(this.getClass());
	protected BrowserContext browserContext;
	protected TestEnvironments environment;

	protected static int INSTANT_WAIT = 200;
	protected static int SHORT_WAIT = 1000;
	protected static int LONG_WAIT = 5000;
	protected static int LOAD_TIMEOUT = 60000;
	protected static int PAGE_OAD_TIMEOUT = 120000;

	public WebApp() {
		log.info("Creating web application");
		environment = TestContextManager.getTestContext().getEnvironment();
		browserContext = new BrowserContext();
	}

	public WebApp getWebApp() {
		return this;
	}

	public Logger getLogger() {
		return log;
	}

	public BrowserContext getBrowserContext() {
		return browserContext;
	}

	public void setBrowserContext(BrowserContext browserContext) {
		this.browserContext = browserContext;
	}

	public TestEnvironments getEnvironment() {
		return environment;
	}

	public void setEnvironment(TestEnvironments environment) {
		this.environment = environment;
	}

	public void quitApp() {
		log.info("Quitting application");

		browserContext.quitApp();
	}

	/**
	 * Takes browser screenshot
	 */
	public void takeScreenshot(String filename) throws Exception {
		try {
			log.info("Taking screenshot...");
			File scrFile;
			// get from remote web driver
			scrFile = ((TakesScreenshot) browserContext.getDriver()).getScreenshotAs(OutputType.FILE);
			if (scrFile == null)
				throw new Exception("Null image file returned");
			File destination = new File("target/screenshots/" + filename + ".jpg");
			FileUtils.copyFile(scrFile, destination);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	/**
	 * Uses Fluent Wait for a specific page to be visible. This calls the
	 * checkOnPage method until timeout
	 */
	public <E extends WebPageBase> WebPageBase waitForPage(Class<E> pageclass) throws Exception {
		log.debug("Wait for Page: [" + pageclass.toString() + "]");
		WebPageBase page = null;
		try {
			// use fluent wait to perform the polling
			browserContext.getDriver().manage().timeouts().implicitlyWait(SHORT_WAIT, TimeUnit.MILLISECONDS);
			page = new FluentWait<>(pageclass).withTimeout(LOAD_TIMEOUT, TimeUnit.MILLISECONDS).pollingEvery(1000, TimeUnit.MILLISECONDS)
					.until(new Function<Class<E>, WebPageBase>() {
						public WebPageBase apply(Class<E> pageclass) {
							try {
								WebPageBase page = pageclass.newInstance();
								page.initPageObject(new Object[] {});
								return page;
							} catch (Exception e) {
								log.warn(e.toString());
								return null;
							}
						}
					});
		} catch (Exception e) {
			throw new Exception("Timed out waiting for page: " + pageclass.getName());
		} finally {
			browserContext.getDriver().manage().timeouts().implicitlyWait(LOAD_TIMEOUT, TimeUnit.SECONDS);
		}
		return page;
	}

	public WebElement safeWait(final By selector) throws Exception {
		return safeWait(selector, LOAD_TIMEOUT);
	}

	/**
	 * Safe wait for single element with a By selector and customized timeout
	 */
	public WebElement safeWait(final By selector, int timeOut) throws Exception {
		log.debug("Wait for Selector: [" + selector.toString() + "]");
		WebElement element = null;
		try {
			browserContext.getDriver().manage().timeouts().implicitlyWait(SHORT_WAIT, TimeUnit.MILLISECONDS);
			FluentWait<WebDriver> wait = new FluentWait<>(browserContext.getDriver()).withTimeout(timeOut, TimeUnit.MILLISECONDS)
					.pollingEvery(SHORT_WAIT, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class)
					.ignoring(StaleElementReferenceException.class);
			element = wait.until(new Function<WebDriver, WebElement>() {
				public WebElement apply(WebDriver driver) {
					WebElement el = driver.findElement(selector);
					if (el.isDisplayed()) {
						log.debug("Element found: " + selector.toString());
						return el;
					} else {
						log.debug("Element not found: " + selector.toString());
						return null;
					}
				}
			});
		} catch (Exception e) {
			throw new Exception("Timed out waiting for web element <" + selector + "> after " + timeOut + " seconds.");
		} finally {
			browserContext.getDriver().manage().timeouts().implicitlyWait(LOAD_TIMEOUT, TimeUnit.SECONDS);
		}
		return element;
	}

	public List<WebElement> safeWaitElements(final By selector) throws Exception {
		return safeWaitElements(selector, LOAD_TIMEOUT);
	}

	/**
	 * Safe wait for list of elements with a By selector and customized timeout
	 */
	public List<WebElement> safeWaitElements(final By selector, int timeOut) throws Exception {
		log.debug("Wait for Selector: [" + selector.toString() + "]");
		List<WebElement> element = null;
		try {
			browserContext.getDriver().manage().timeouts().implicitlyWait(SHORT_WAIT, TimeUnit.MILLISECONDS);
			FluentWait<WebDriver> wait = new FluentWait<>(browserContext.getDriver()).withTimeout(timeOut, TimeUnit.MILLISECONDS)
					.pollingEvery(SHORT_WAIT, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class)
					.ignoring(StaleElementReferenceException.class);
			element = wait.until(new Function<WebDriver, List<WebElement>>() {
				public List<WebElement> apply(WebDriver driver) {
					List<WebElement> el = driver.findElements(selector);
					if (el.size() > 0) {
						log.debug("Elements found: " + selector.toString());
						return el;
					} else {
						log.debug("Elements not found: " + selector.toString());
						return null;
					}
				}
			});
		} catch (Exception e) {
			throw new Exception("Timed out waiting for web element <" + selector + "> after " + timeOut + " seconds.");
		} finally {
			browserContext.getDriver().manage().timeouts().implicitlyWait(LOAD_TIMEOUT, TimeUnit.SECONDS);
		}
		return element;
	}

	/**
	 * Performs an action on a web element with wait limited by a custom
	 * timeout. Selector (By) is used rather than WebElement. ElementActions
	 * enum is used instead of direct passing of a method name due to the
	 * Reflect`s inability to find WebElement`s methods. args[] is used for
	 * invoking methods with arguments, like sendKeys() The best way to make it
	 * process a new method is to add a one more "if (action == ...)" below and
	 * a new ElementActions item for it.
	 */
	public void safeAction(By selector, ElementActions action, Object args[]) throws Exception {
		log.debug("Action: [" + action.toString() + "] Selector: [" + selector.toString() + "]");
		try {
			browserContext.getDriver().manage().timeouts().implicitlyWait(SHORT_WAIT, TimeUnit.MILLISECONDS);
			Object[] params = new Object[] { browserContext.getDriver(), selector, action, args };
			FluentWait<Object[]> wait = new FluentWait<Object[]>(params).withTimeout(LOAD_TIMEOUT, TimeUnit.MILLISECONDS).pollingEvery(SHORT_WAIT,
					TimeUnit.MILLISECONDS);
			Function<Object[], Boolean> function = new Function<Object[], Boolean>() {
				public Boolean apply(Object[] params) {
					try {
						WebDriver driver = (WebDriver) params[0];
						By selector = (By) params[1];
						ElementActions action = (ElementActions) params[2];
						Object[] args = (Object[]) params[3];
						// find element
						WebElement el = driver.findElement(selector);
						if (el.isDisplayed() || el.isEnabled()) {
							if (action == ElementActions.CLICK) {
								el.click();
							} else if (action == ElementActions.CLEAR) {
								el.clear();
							} else if (action == ElementActions.SEND_KEYS) {
								log.debug("Entering [" + args[0].toString() + "]");
								el.sendKeys((String) args[0]);
							} else if (action == ElementActions.SELECT_OPTION) {
								Select select = new Select(el);
								select.selectByVisibleText((String) args[0]);
							} else if (action == ElementActions.HOVER) {
								new Actions(driver).moveToElement(el).perform();
							} else if (action == ElementActions.SCROLL) {
								WebElement element = driver.findElement(selector);
								((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
							}
							return true;
						} else {
							return false;
						}
					} catch (Exception e) {
						return false;
					}
				}
			};
			wait.until(function);
		} catch (Exception e) {
			throw new Exception("Timed out waiting for web element <" + selector + ">");
		} finally {
			browserContext.getDriver().manage().timeouts().implicitlyWait(LOAD_TIMEOUT, TimeUnit.SECONDS);
		}
	}

	/**
	 * Safely wait and get text of an element
	 */
	public String safeGetText(final By selector) throws Exception {
		log.debug("Get text from Selector: [" + selector.toString() + "]");
		String result = null;
		try {
			browserContext.getDriver().manage().timeouts().implicitlyWait(SHORT_WAIT, TimeUnit.MILLISECONDS);
			FluentWait<WebDriver> wait = new FluentWait<>(browserContext.getDriver()).withTimeout(LOAD_TIMEOUT, TimeUnit.MILLISECONDS)
					.pollingEvery(SHORT_WAIT, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class)
					.ignoring(StaleElementReferenceException.class);
			result = wait.until(new Function<WebDriver, String>() {
				public String apply(WebDriver driver) {
					return driver.findElement(selector).getText().trim();
				}
			});
		} catch (Exception e) {
			throw new Exception("Timed out waiting for web element: " + selector);
		} finally {
			browserContext.getDriver().manage().timeouts().implicitlyWait(LOAD_TIMEOUT, TimeUnit.SECONDS);
		}
		return result;
	}

	/**
	 * Safely wait for an element to disappear
	 */
	public void safeElementWaitToDisappear(final By selector) throws Exception {
		log.debug("Wait to disappear Selector: [" + selector.toString() + "]");
		try {
			browserContext.getDriver().manage().timeouts().implicitlyWait(SHORT_WAIT, TimeUnit.MILLISECONDS);
			FluentWait<WebDriver> wait = new FluentWait<>(browserContext.getDriver()).withTimeout(LOAD_TIMEOUT, TimeUnit.MILLISECONDS)
					.pollingEvery(SHORT_WAIT, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class)
					.ignoring(StaleElementReferenceException.class);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(selector));
		} catch (Exception e) {
			throw new Exception("Timed out waiting for web element to disappear <" + selector + ">");
		} finally {
			browserContext.getDriver().manage().timeouts().implicitlyWait(LOAD_TIMEOUT, TimeUnit.SECONDS);
		}
	}

	/**
	 * Safely clicking on an element
	 */
	public void safeClick(By selector) throws Exception {
		safeAction(selector, ElementActions.CLICK, new Object[] {});
	}

	public void safeInput(By selector, String keys) throws Exception {
		safeAction(selector, ElementActions.SEND_KEYS, new Object[] { keys });
	}

	public void safeSelect(By selector, String value) throws Exception {
		safeAction(selector, ElementActions.SELECT_OPTION, new Object[] { value });
	}

	public void safeClear(By selector) throws Exception {
		safeAction(selector, ElementActions.CLEAR, new Object[] {});
	}

	public void safeHover(By selector) throws Exception {
		safeAction(selector, ElementActions.HOVER, new Object[] {});
	}

	public void safeScroll(By selector) throws Exception {
		safeAction(selector, ElementActions.SCROLL, new Object[] {});
	}

	/**
	 * Quick-fail method to check the given element is not enabled. This is to
	 * avoid long waiting time when checking for an element
	 */
	public boolean isElementEnabled(final By selector) throws Exception {
		return safeWait(selector).isEnabled();
	}

	/**
	 * Quick-fail method to check the given element is not displayed. This is to
	 * avoid long waiting time when checking for an element
	 */
	public boolean isElementDisplayed(final By selector) throws Exception {
		try {
			safeWait(selector, LONG_WAIT);
		} catch (Exception e) {
			log.debug("Element " + selector.toString() + " is not displayed");
			return false;
		}
		log.debug("Element [" + selector + "] is displayed");
		return true;
	}

	/**
	 * Wait for all jQuery requests to be finished
	 */
	public void waitForJQueryCalls() {
		try {
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(browserContext.getDriver()).withTimeout(LOAD_TIMEOUT, TimeUnit.MILLISECONDS).pollingEvery(SHORT_WAIT,
					TimeUnit.MILLISECONDS);
			wait.until(new Function<WebDriver, Object>() {
				@Override
				public Boolean apply(WebDriver driver) {
					try {
						return (Boolean) ((JavascriptExecutor) driver).executeScript("return (window.jQuery != null) && (jQuery.active == 0)");
					} catch (Exception e) {
						return false;
					}
				}
			});
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	/**
	 * Wait for all ajax requests to be finished
	 */
	public void waitForActiveAjaxRequest() {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(browserContext.getDriver()).withTimeout(LOAD_TIMEOUT, TimeUnit.MILLISECONDS)
				.pollingEvery(SHORT_WAIT, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException.class);
		wait.until(new Function<Object, Object>() {
			@Override
			public Boolean apply(Object input) {
				try {
					return (Boolean) ((JavascriptExecutor) browserContext.getDriver()).executeScript("return Ajax.activeRequestCount == 0");
				} catch (JavascriptException e) {
					return true;
				} catch (Exception e) {
					return false;
				}
			}
		});
	}

	/**
	 * Wait for all java scripts to finish processing
	 */
	public void waitForDom() {
		try {
			FluentWait<WebDriver> wait = new FluentWait<WebDriver>(browserContext.getDriver()).withTimeout(LOAD_TIMEOUT, TimeUnit.MILLISECONDS).pollingEvery(SHORT_WAIT,
					TimeUnit.MILLISECONDS);
			wait.until(new Function<WebDriver, Object>() {
				@Override
				public Boolean apply(WebDriver driver) {
					try {
						return ((JavascriptExecutor) browserContext.getDriver()).executeScript("return document.readyState").toString().equals("complete");
					} catch (Exception e) {
						return false;
					}
				}
			});
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	/**
	 * Wait for all Ajax and jQuery calls until they are finished
	 */
	public void waitForAllCalls() {
		log.debug("Waiting for calls");
		waitForDom();
		waitForJQueryCalls();
		waitForActiveAjaxRequest();
	}
}
