package com.steps;

import org.joda.time.DateTime;

import com.pageobjects.common.PurchaseSteps;
import com.pageobjects.tiki.TikiCartPage;
import com.pageobjects.tiki.TikiCategoryPage;
import com.pageobjects.tiki.TikiCheckoutPage;
import com.pageobjects.tiki.TikiHeader;
import com.pageobjects.tiki.TikiHomePage;
import com.pageobjects.tiki.TikiProductPage;
import com.testdata.TestData;
import com.unity.Domain;

import cucumber.api.java.en.And;

public class TikiSteps extends PurchaseSteps {
	TikiHeader tikiHeader;
	TikiHomePage tikiHomePage;
	TikiCategoryPage tikiCategoryPage;
	TikiProductPage tikiProductPage;
	TikiCartPage tikiCartPage;
	TikiCheckoutPage tikiCheckoutPage;

	public TikiSteps() throws Throwable {
		super();
	}

	// Initialize pages
	public TikiHeader getTikiHeader() throws Exception {
		TikiHeader page = (TikiHeader) app.waitForPage(TikiHeader.class);
		tikiHeader = page;
		return page;
	}

	public TikiHomePage getTikiHomePage() throws Exception {
		TikiHomePage page = pageIsTheSame() && tikiHomePage != null ? tikiHomePage : (TikiHomePage) app.waitForPage(TikiHomePage.class);
		tikiHomePage = page;
		return page;
	}
	
	public TikiCategoryPage getTikiCategoryPage() throws Exception {
		TikiCategoryPage page = pageIsTheSame() && tikiCategoryPage != null ? tikiCategoryPage : (TikiCategoryPage) app.waitForPage(TikiCategoryPage.class);
		tikiCategoryPage = page;
		return page;
	}


	public TikiCartPage getTikiCartPage() throws Exception {
		TikiCartPage page = pageIsTheSame() && tikiCartPage != null ? tikiCartPage : (TikiCartPage) app.waitForPage(TikiCartPage.class);
		tikiCartPage = page;
		return page;
	}

	public TikiProductPage getTikiProductPage() throws Exception {
		TikiProductPage page = pageIsTheSame() && tikiProductPage != null ? tikiProductPage
				: (TikiProductPage) app.waitForPage(TikiProductPage.class);
		tikiProductPage = page;
		return page;
	}

	public TikiCheckoutPage getTikiCheckoutPage() throws Exception {
		TikiCheckoutPage page = pageIsTheSame() && tikiCheckoutPage != null ? tikiCheckoutPage
				: (TikiCheckoutPage) app.waitForPage(TikiCheckoutPage.class);
		tikiCheckoutPage = page;
		return page;
	}

	// Define steps
	@And("^I select first recommended product on Homepage of Tiki$")
	public void iSelectFirstRecommendedProductOnHomePage() throws Throwable {
		tikiHomePage = getTikiHomePage();
		if (tikiHomePage.isPopupDisplayed())
			tikiHomePage.closePopup();
		log.info("Step: I select first recommended product on Homepage of Tiki");
		tikiHomePage.clickRecommendedItem();
	}
	
	@And("^I select first recommended product on Category Page of Tiki$")
	public void iSelectFirstRecommendedProductOnCategoryPage() throws Throwable {
		tikiCategoryPage = getTikiCategoryPage();
		log.info("Step: I select first recommended product on Category page of Tiki");
		tikiCategoryPage.clickRecommendedItem();
	}

	@And("^I add product to cart on Tiki$")
	public void iAddProductToCart() throws Throwable {
		log.info("Step: Adding product to cart");
		tikiProductPage = getTikiProductPage();
		tikiProductPage.clickAddToCart();
	}

	@And("^I proceed to checkout page on Tiki$")
	public void iProceedToCheckoutPage() throws Throwable {
		log.info("Step: I proceed to checkout page on Tiki");
		tikiHeader = getTikiHeader();
		tikiHeader.clickOpenCart();
		tikiCartPage = getTikiCartPage();
		tikiCartPage.clickToCheckout();
	}

	@And("^I log in at checkout page on Tiki$")
	public void iSelectLogin() throws Throwable {
		log.info("I log in at checkout page on Tiki");
		tikiCheckoutPage = getTikiCheckoutPage();
		tikiCheckoutPage.clickLogInTab();
		tikiCheckoutPage.inputEmailPassword(TestData.readUser(Domain.TIKI, "EMAIL"),
				TestData.readUser(Domain.TIKI, "PASSWORD"));
		tikiCheckoutPage.clickLogInButton();
	}

	@And("^I select address at checkout page on Tiki$")
	public void iSelectAddress() throws Throwable {
		log.info("I select address at checkout page on Tiki");
		tikiCheckoutPage = getTikiCheckoutPage();
		if (tikiCheckoutPage.isAddressSaved())
			tikiCheckoutPage.clickSelectThisAddress();
	}

	@And("^I cancel payment on Tiki$")
	public void iFinishPayment() throws Throwable {
		log.info("I finish payment on Tiki");
		tikiCheckoutPage = getTikiCheckoutPage();
		order.setPurchaseValue(tikiCheckoutPage.getTotalAmount());
	}
}
