package com.steps;

import com.pageobjects.common.PurchaseSteps;
import com.testdata.TestData;
import com.unity.Domain;
import com.unity.Purpose;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;

public class CommonSteps extends PurchaseSteps {
	public CommonSteps() throws Throwable {
	}

	@And("^I wait (\\d+) minutes$")
	public void iWaitMinutes(int timeWait) throws Exception {
		try {
			log.info("Waiting for " + timeWait + " minutes");
			Thread.sleep(timeWait * 60000);
			log.info("Getting awake");
		} catch (Exception e) {
			log.warn(e.toString());
		}
	}
	
	/**
	 * Refresh the page
	 */
	@And("^I refresh the page$")
	public void refresh() {
		try {
			log.info("Refreshing the page");
			app.getBrowserContext().getDriver().navigate().refresh();
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@And("^I open full product link to (.*?)$")
	public void iOpenFullLink(String site) throws Exception {
		String link;
		app.getContext().setPurpose(Purpose.PURCHASE);
		switch (site.toLowerCase()) {
			case "tiki":
				link = TestData.readLink(Domain.TIKI, "PRODUCT_LINK");
				break;
			case "lotte":
				link = TestData.readLink(Domain.LOTTE, "PRODUCT_LINK");
				break;
			default:
				link = "";
				throw new PendingException("Unrecognised site: " + site.toString());
		}
		log.info("Step: I open full product link to: " + site + " - " + link);
		app.getBrowserContext().getDriver().get(link);
		order.setProductLink(link);
		order.setIsValidated(0);
		app.waitForAllCalls();
	}
}
