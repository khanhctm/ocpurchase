package com.steps;

import com.pageobjects.common.PurchaseSteps;
import com.pageobjects.lotte.LotteCartPage;
import com.pageobjects.lotte.LotteCheckoutPage;
import com.pageobjects.lotte.LotteHomePage;
import com.pageobjects.lotte.LotteProductPage;
import com.testdata.TestData;
import com.unity.Domain;

import cucumber.api.java.en.And;

public class LotteSteps extends PurchaseSteps {

    LotteProductPage lotteProductpage;
    LotteCartPage lotteCartPage;
    LotteCheckoutPage lotteCheckoutPage;
    LotteHomePage lotteHomePage;

    public LotteSteps() throws Throwable {
        super();
    }

    public LotteHomePage getLotteHomePage() throws Exception {
        LotteHomePage page = pageIsTheSame() && lotteHomePage != null ? lotteHomePage : (LotteHomePage) app.waitForPage(LotteHomePage.class);
        lotteHomePage = page;
        return page;
    }

    public LotteProductPage getLotteProductPage() throws Exception {
        LotteProductPage page = pageIsTheSame() && lotteProductpage != null ? lotteProductpage
                : (LotteProductPage) app.waitForPage(LotteProductPage.class);
        lotteProductpage = page;
        return page;
    }

    public LotteCartPage getLotteCartPage() throws Exception {
        LotteCartPage page = pageIsTheSame() && lotteCartPage != null ? lotteCartPage
                : (LotteCartPage) app.waitForPage(LotteCartPage.class);
        lotteCartPage = page;
        return page;
    }

    public LotteCheckoutPage getLotteCheckoutPage() throws Exception {
        LotteCheckoutPage page = pageIsTheSame() && lotteCheckoutPage != null ? lotteCheckoutPage
                : (LotteCheckoutPage) app.waitForPage(LotteCheckoutPage.class);
        lotteCheckoutPage = page;
        return page;
    }

    @And("^I select first recommended product on Category Page of Lotte$")
    public void iselectfirstsuggestproduct() throws Throwable {
        lotteHomePage = getLotteHomePage();
        log.info("Steps: Click first suggestion item on Lotte home page");
        lotteHomePage.clickRecommendedItem();
    }

    @And("^I add click checkout button on Lotte Product page$")
    public void iAddProductToCart() throws Throwable {
        lotteProductpage = getLotteProductPage();
        log.info("Step: Click checkout button");
        lotteProductpage.clickCheckOutButton();
    }

    @And("^I proceed to checkout page of Lotte$")
    public void iProceedToCheckoutPage() throws Throwable {
        log.info("Step: I proceed to checkout page on Lotte");
        getLotteCartPage().clickToCheckout();
    }

    @And("^I log in at cart page on Lotte$")
    public void iSelectLogin() throws Throwable {
        lotteCartPage = getLotteCartPage();
        log.info("I log in at cart page on lotte");
        lotteCartPage.inputEmailPassword(TestData.readUser(Domain.LOTTE, "EMAIL"),
                TestData.readUser(Domain.LOTTE, "PASSWORD"));
        lotteCartPage.clickLogInButton();
    }

    @And("^I select address at checkout page on Lotte$")
    public void iSelectAddress() throws Throwable {

        lotteCheckoutPage = getLotteCheckoutPage();
        log.info("I select address at checkout page on Lotte");
        log.info("I click next button at checkout page on Lotte");
        lotteCheckoutPage.clickNextOnShippingDetail();
        log.info("I click next 1 button at checkout page on Lotte");
        lotteCheckoutPage.clickNextOnDeliveryDetail();
    }

    @And("^I choose payment method and cancel payment on Lotte$")
    public void iCancelPayment() throws Throwable {
        lotteCheckoutPage = getLotteCheckoutPage();
        log.info("I choose COD method");
        lotteCheckoutPage.chooseCOD();
        log.info("I cancel payment on lotte");
    }
}
	
	

