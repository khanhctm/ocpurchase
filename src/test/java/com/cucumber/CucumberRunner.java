package com.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(glue = {"com.context", "com.steps", "com.automation.base" }, features = { "src/test/scenarios/" }, plugin = { "pretty", "html:target/cucumber", "json:target/cucumber.json", "junit:target/junit.xml"})
public class CucumberRunner {
	
}
