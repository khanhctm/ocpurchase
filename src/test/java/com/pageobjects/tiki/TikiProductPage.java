package com.pageobjects.tiki;

import org.openqa.selenium.By;

import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;

public class TikiProductPage extends WebPageBase {

	private static final By PRODUCT_PAGE = By.cssSelector(".tiki-product");
	private static final By ADD_TO_CART = By.cssSelector(".cta-box button.add-to-cart");
	private static final By POPUP_OVERLAY_CLOSE = By.cssSelector("#ematic_closeExitIntentOverlay_1_xl_1_2");

	public TikiProductPage() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(PRODUCT_PAGE);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void closeAnyPopup() throws Exception {
		try {
			if (app.isElementDisplayed(POPUP_OVERLAY_CLOSE)) {
				app.safeClick(POPUP_OVERLAY_CLOSE);
			}
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickAddToCart() throws Exception {
		try {
			app.safeClick(ADD_TO_CART);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}
}
