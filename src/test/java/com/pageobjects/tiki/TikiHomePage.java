package com.pageobjects.tiki;

import org.openqa.selenium.By;

import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;

public class TikiHomePage extends WebPageBase {
	private static final By HOMEPAGE = By.cssSelector(".tiki-home");
	private static final By OVERLAY_BACKGROUND = By.cssSelector("#ematic_background_overlay");
	private static final By POPUP_OVERLAY_CLOSE = By
			.cssSelector("div[style*='display: block;']>input[id^=ematic_closeExitIntentOverlay]");
	private static final By RECOMENDED_ITEM_ACTIVE = By.cssSelector(".product-author-content .swiper-slide-active a");

	public TikiHomePage() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(HOMEPAGE);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public boolean isPopupDisplayed() throws Exception {
		try {
			return app.isElementDisplayed(POPUP_OVERLAY_CLOSE);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void closePopup() throws Exception {
		try {
			app.safeClick(POPUP_OVERLAY_CLOSE);
			app.safeElementWaitToDisappear(OVERLAY_BACKGROUND);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickRecommendedItem() throws Exception {
		try {
			app.safeScroll(RECOMENDED_ITEM_ACTIVE);
			app.safeHover(RECOMENDED_ITEM_ACTIVE);
			Thread.sleep(1000);
			app.safeClick(RECOMENDED_ITEM_ACTIVE);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}

	}

}
