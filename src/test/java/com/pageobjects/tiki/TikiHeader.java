package com.pageobjects.tiki;

import org.openqa.selenium.By;

import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;

public class TikiHeader extends WebPageBase {
	private static final By HEADER = By.cssSelector("#header");
	private static final By LOGO = By.cssSelector(".navbar-brand");
	private static final By CART = By.cssSelector(".header-cart.item");
	private static final By CART_COUNT = By.cssSelector(".cart-count");

	public TikiHeader() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(HEADER);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickLogo() throws Exception {
		try {
			app.safeClick(LOGO);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	public void clickOpenCart() throws Exception {
		try {
			app.safeClick(CART);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int getCartCount() throws Exception {
		try {
			return Integer.parseInt(app.safeWait(CART_COUNT).getText());
		} catch (NullPointerException e) {
			return 0;
		} catch (NumberFormatException e) {
			return 0;
		}
	}
}
