package com.pageobjects.lotte;

import org.openqa.selenium.By;

import com.automation.base.WebPageBase;
import com.automation.core.WebApp;
import com.automation.core.WebAppManager;

public class LotteHomePage extends WebPageBase {
	private static final By HOMEPAGE_LOADED = By.cssSelector(".section-highlight .owl-loaded");
	private static final By RECOMENDED_ITEM_ACTIVE = By.cssSelector(".products-tab-content .item-product a");

	public LotteHomePage() throws Exception {
		super();
	}

	@Override
	protected void init(Object[] params) throws Exception {
		try {
			app = WebAppManager.getWebApp(WebApp.class);
			driver = app.getBrowserContext().getDriver();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	@Override
	protected void checkOnPage() throws Exception {
		try {
			app.safeWait(HOMEPAGE_LOADED);
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}
	}

	
	public void clickRecommendedItem() throws Exception {
		try {
			app.safeScroll(RECOMENDED_ITEM_ACTIVE);
			app.safeHover(RECOMENDED_ITEM_ACTIVE);
			Thread.sleep(1000);
			app.safeClick(RECOMENDED_ITEM_ACTIVE);
			app.waitForAllCalls();
		} catch (Exception e) {
			log.error(e.toString());
			throw e;
		}

	}

}
